//
//  RCTRectangleViewManager.m
//  RNPlayground
//
//  Created by Bogdan Iusco on 3/16/16.
//  Copyright © 2016 Facebook. All rights reserved.
//

#import "RCTRectangleViewManager.h"
#import "RNPlayground-Swift.h"

@implementation RCTRectangleViewManager

RCT_EXPORT_MODULE()

- (UIView *)view {
  return [RectangleView new];
}

RCT_EXPORT_VIEW_PROPERTY(rectangleTopLeftCornerInset, CGPoint)
RCT_EXPORT_VIEW_PROPERTY(rectangleTopRightCornerInset, CGPoint)
RCT_EXPORT_VIEW_PROPERTY(rectangleBottomLeftCornerInset, CGPoint)
RCT_EXPORT_VIEW_PROPERTY(rectangleBottomRightCornerInset, CGPoint)
RCT_EXPORT_VIEW_PROPERTY(rectangleFillColor, UIColor)

@end
