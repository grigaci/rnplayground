//
//  RectangleView.swift
//  RNPlayground
//
//  Created by Bogdan Iusco on 3/16/16.
//  Copyright © 2016 Facebook. All rights reserved.
//

import Foundation

import UIKit

@objc(RectangleView)
class RectangleView: UIView {
  
  var rectangleTopLeftCornerInset: CGPoint = CGPointZero
  var rectangleTopRightCornerInset: CGPoint = CGPointZero
  var rectangleBottomLeftCornerInset: CGPoint = CGPointZero
  var rectangleBottomRightCornerInset: CGPoint = CGPointZero
  
  var rectangleFillColor: UIColor = UIColor.whiteColor()
  var rectangleShapeLayer: CAShapeLayer = CAShapeLayer()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    self.rectangleShapeLayer.frame = self.bounds
    self.redrawRectangle()
  }
  
  func redrawRectangle() {
    self.rectangleShapeLayer.fillColor = self.rectangleFillColor.CGColor
    self.rectangleShapeLayer.strokeColor = UIColor.clearColor().CGColor
    self.rectangleShapeLayer.path = self.calculateRectanglePath()
    if self.rectangleShapeLayer.superlayer == nil {
      self.layer.addSublayer(self.rectangleShapeLayer)
    }
  }
  
  private func calculateRectanglePath() -> CGPath {
    let path = UIBezierPath()
    let width = CGRectGetWidth(self.bounds)
    let height = CGRectGetHeight(self.bounds)
    
    let topLeftCorner  = CGPointMake(self.rectangleTopLeftCornerInset.x,self.rectangleTopLeftCornerInset.y)
    let topRightCorner = CGPointMake(width - self.rectangleTopRightCornerInset.x, self.rectangleTopRightCornerInset.y)
    let bottomLeftCorner = CGPointMake(self.rectangleBottomLeftCornerInset.x, height - self.rectangleBottomLeftCornerInset.y)
    let bottomRightCorner = CGPointMake(width - self.rectangleBottomRightCornerInset.x, height - self.rectangleBottomRightCornerInset.y)
    
    path.moveToPoint(topLeftCorner)
    path.addLineToPoint(topRightCorner)
    path.addLineToPoint(bottomRightCorner)
    path.addLineToPoint(bottomLeftCorner)
    path.addLineToPoint(topLeftCorner)
    path.closePath()
    
    return path.CGPath
  }
}
