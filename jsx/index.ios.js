/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View
} from 'react-native';

var RectangleView = require('./RectangleView');

class RNPlayground extends Component {
  render() {
    var topLeftCorner     = {x:10, y:40};
    var topRightCorner    = {x:10, y:20};
    var bottomLeftCorner  = {x:10, y:10};
    var bottomRightCorner = {x:10, y:10};

    return (
      <RectangleView style={styles.container}
                     rectangleTopLeftCornerInset     = {topLeftCorner}
                     rectangleTopRightCornerInset    = {topRightCorner}
                     rectangleBottomLeftCornerInset  = {bottomLeftCorner}
                     rectangleBottomRightCornerInset = {bottomRightCorner}
                     rectangleFillColor = 'blue'>
      </RectangleView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
    height:400
  }
});

AppRegistry.registerComponent('RNPlayground', () => RNPlayground);
