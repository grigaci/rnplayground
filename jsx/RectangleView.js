import React, { ColorPropType, PointPropType, requireNativeComponent } from 'react-native';

class RectangleView extends React.Component {
  render() {
    return <RCTRectangleView {...this.props}/>;
  }
}

RectangleView.propTypes = {
	rectangleTopLeftCornerInset:     PointPropType,
	rectangleTopRightCornerInset:    PointPropType,
	rectangleBottomLeftCornerInset:  PointPropType,
	rectangleBottomRightCornerInset: PointPropType,
	rectangleFillColor: 			 ColorPropType
};

var RCTRectangleView = requireNativeComponent('RCTRectangleView', RectangleView);

module.exports = RectangleView;